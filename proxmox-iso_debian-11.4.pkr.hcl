source "proxmox" "Debian11" {
  boot_command        = var.boot_command
  boot_wait           = var.boot_wait
  cores               = var.cores
  sockets             = var.sockets
  cpu_type            = var.cpu_type
  scsi_controller     = var.scsi_controller
  os                  = var.os
  
  disks {
    disk_size         = var.disk_size
    storage_pool      = var.storage_pool
    storage_pool_type = var.storage_pool_type
    format            = var.format
    type              = var.type
    #ssd               = var.ssd
    io_thread         = var.io_thread
  }

  http_directory           = var.http_directory
  insecure_skip_tls_verify = true
  iso_file                 = var.iso_file
  memory                   = var.memory
  
  network_adapters {
    bridge      = var.bridge
    model       = var.model
    firewall    = var.firewall
    mac_address = var.mac_address
    #vlan_tag    = var.vlan_tag
  }
  
  node                   = var.proxmox_node
  username               = var.proxmox_username
  password               = var.proxmox_password
  proxmox_url            = var.proxmox_url
  qemu_agent             = var.qemu_agent
  onboot                 = var.onboot
  unmount_iso            = var.unmount_iso
  vm_name                = var.vm_name

  ssh_handshake_attempts = var.ssh_handshake_attempts
  ssh_password           = var.ssh_password
  ssh_timeout            = var.ssh_timeout
  ssh_username           = var.ssh_username
  template_description   = var.template_description
  template_name          = var.template_name
}


build {
    sources = [
        "proxmox.Debian11",
    ]

  provisioner "shell-local" {
    inline  = ["echo the address is: $PACKER_HTTP_ADDR and build name is: $PACKER_BUILD_NAME"]
  }
} 
