### Proxmox Node ###
variable "proxmox_node" {}
variable "proxmox_url" {}
variable "proxmox_username" {}
variable "proxmox_password" {}

### iso Config ### 
variable "iso_file" {}
variable "boot_command" {}
variable "boot_wait" {}
variable "http_directory" {}

### Template ###
################
# VM Config #
variable "cores" {}
variable "memory" {}
variable "vm_name" {}
variable "sockets" {}
variable "cpu_type" {}

#Disks
variable "disk_size" {}
variable "storage_pool" {}
variable "storage_pool_type" {}
variable "scsi_controller" {}
variable "type" {}
variable "unmount_iso" {}
variable "format" {}
variable "io_thread" {}
variable "ssd" {}

#Network
variable "bridge" {}
variable "mac_address" {}
variable "firewall" {}
variable "model" {}
#variable "vlan_tag" {}

#Info
variable "template_description" {}
variable "template_name" {}

#ssh
variable "ssh_handshake_attempts" {}
variable "ssh_username" {}
variable "ssh_password" {}
variable "ssh_timeout" {}
variable "qemu_agent" {}
variable "onboot" {}
variable "os" {}
