### Proxmox Node ###
proxmox_node                = "pve"
proxmox_url                 = "https://192.168.8.2:8006/api2/json"
proxmox_username            = "root@pam"
proxmox_password            = "password"

### iso Config ### 
iso_file            = "local:iso/debian-11.4.0-amd64-netinst.iso"
#boot_command        = ["<esc><wait>", "vmlinuz initrd=initrd.img inst.geoloc=0 rd.driver.blacklist=dm-multipath net.ifnames=0 biosdevname=0", "inst.text ip=192.168.8.32::192.168.1.63:255.255.255.192:template:eth0:none nameserver=192.168.8.30 inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg <enter>"]
boot_command = [
        "<esc><wait>",
        "install <wait>",
        " netcfg/disable_dhcp=true netcfg/confirm_static=true netcfg/get_ipaddress=192.168.8.55 netcfg/get_netmask=255.255.255.192 netcfg/get_gateway=192.168.8.30 netcfg/get_nameservers=192.168.8.30 preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<wait>",
        " auto-install/enable=true ",
        " locale=en_US.UTF-8 <wait>",
        " priority=critical",
        "<enter><wait>"
  ]
boot_wait           = "3s"
http_directory      = "http"

### Template ###
################
# VM Config #
memory      = 2048
vm_name     = "Debian-11.4"
cores       = 2
sockets     = 1
#cpu_type    = "kvm64"
cpu_type    = "host"

#Disks
disk_size               = "30"
storage_pool            = "local-lvm"
storage_pool_type       = "lvm"
#scsi_controller         = "virtio-scsi-pci"
scsi_controller         = "virtio-scsi-single"
type                    = "virtio"
format                  = "raw"
io_thread               = "true"
ssd                     = "true"
#Network
bridge                  = "vmbr0"
mac_address             = "e6:5b:af:d2:6c:94"
firewall                = "true"
model                   = "e1000"
#vlan_tag                = 3001

#Info
template_description    = "Debian-11.4"
template_name           = "Template-Debian-11.4"

#ssh
ssh_handshake_attempts  = "100"
ssh_username            = "packer"
ssh_password            = "packer"
ssh_timeout             = "40m"
qemu_agent              = "true"
onboot                  = "true"
unmount_iso             = "true"
os                      = "l26"
